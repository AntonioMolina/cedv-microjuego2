#ifndef Player_H
#define Player_H

#include "Constants.h"
#include "Node.h"

class Player {
    public:
        Player (Node _nodeInicio);
        ~Player ();

        void die();

        int getLifes () { return _lifes; }
        Node getNodeActual () { return _nodeActual; }
        Node getNodeSiguiente () { return _nodeSiguiente; }
        Sentido getSentidoActual () { return _sentActual; }
        Sentido getSentidoSiguiente () { return _sentSiguiente; }
        Sentido getMiraA () { return _miraA; }

        void setNodeActual (Node nAct) { _nodeActual = nAct; }
        void setNodeSiguiente (Node nSig) { _nodeSiguiente = nSig; }
        void setSentActual (Sentido s) {
            _sentActual = s;
            if (s != stop) {
                _miraA = s;
            }
        }
        void setSentSiguiente (Sentido s) { _sentSiguiente = s; }

    private:
        int _lifes;

        Node _nodeActual;
        Node _nodeSiguiente;

        Sentido _sentActual;
        Sentido _sentSiguiente;

        Sentido _miraA;
};

#endif /* Player_H */
