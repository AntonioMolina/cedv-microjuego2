#ifndef Ghost_H
#define Ghost_H

#include "Node.h"
#include "Constants.h"

class Ghost {
    public:
        Ghost (Node _nodeInicio, int id);
        ~Ghost();

        int getId () { return _id; }
        Node getNodeActual () { return _nodoActual; }
        Node getNodeSiguiente () { return _nodeSiguiente; }
        Sentido getSentidoActual () { return _sentidoActual; }

        void setId (int id) { _id = id; }
        void setNodeActual (Node nodoActual) { _nodoActual=nodoActual; }
        void setNodeSiguiente (Node nSig) { _nodeSiguiente = nSig; }
        void setSentidoActual (Sentido sentidoActual) { _sentidoActual=sentidoActual; }

    private:
        int _id;

        Node _nodoActual;
        Node _nodeSiguiente;
        Sentido _sentidoActual;
};

#endif
