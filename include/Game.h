#ifndef Game_H
#define Game_H

#include <Ogre.h>

#include "Constants.h"
#include "Ghost.h"
#include "Importer.h"
#include "Node.h"
#include "Player.h"
#include "Scene.h"

class Game {
    public:
        Game ();
        ~Game ();

        int getLifes ();
        Player* getPlayer () { return _player; }
        int getPuntuation () { return _puntuation; }
        std::vector<Sentido> getPossibleSentidos (Node nAct);
        Scene* getScene () { return _scene; }
        std::vector<Ghost*> getGhosts () { return _ghosts; }
        Ghost* getGhost (int index) { return _ghosts.at(index); }

        void eatBall ();
        bool checkLose ();
        bool checkWin ();
        bool checkColission ();
        bool checkColission (int id);

        void movePlayer (Ogre::Vector3 v);
        void moveGhost (Ogre::Vector3 v, int id);
        void playerDie ();
        void respawnPacMan ();
        void spawnBola (Ogre::SceneNode* nodeBola, Ogre::Vector3 pos);
        void respawnGhosts ();

        void addPuntuation (int punts);

        void changeColorToBlue ();
        void changeToOriginalColor ();

    protected:
        Ogre::Light* _light;
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;

    private:
        int _puntuation;
        int _nTotalBolas;

        Ogre::SceneNode* _nodeJuego;
        Ogre::SceneNode* _nodePacMan;
        Ogre::SceneNode* _nodeTablero;

        Importer* _importer;
        Scene* _scene;

        Player* _player;
        std::vector<Ghost*> _ghosts;

        void createTable ();
        void createLights ();
        void createPacman ();
		void createBolasGrandes ();
        void createBolasPeques ();
        void createGhosts ();

		void createBackground ();
        void deleteBackground ();
};

#endif /* Game_H */
