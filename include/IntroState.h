#ifndef IntroState_H
#define IntroState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"

class IntroState : public Ogre::Singleton<IntroState>, public GameState
{
    public:
        IntroState ();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        /* Heredados de Ogre::Singleton. */
        static IntroState& getSingleton ();
        static IntroState* getSingletonPtr ();

    protected:
        CEGUI::OgreRenderer* _renderer;
        CEGUI::Window* _sheet;

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;

    private:
        void setCEGUI ();
        void createGUI ();
        void deleteGUI ();

        bool start (const CEGUI::EventArgs &e);
        bool quit (const CEGUI::EventArgs &e);
        bool credits (const CEGUI::EventArgs &e);
        bool records (const CEGUI::EventArgs &e);
        bool controls (const CEGUI::EventArgs &e);
};

#endif /* IntroState_H */
