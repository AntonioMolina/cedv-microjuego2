/* **********************************************************
** Importador NoEscape 1.0
** Curso de Experto en Desarrollo de Videojuegos
** Escuela Superior de Informatica - Univ. Castilla-La Mancha
** Carlos Gonzalez Morcillo - David Vallejo Fernandez
************************************************************/

#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <Ogre.h>
#include <OgreVector3.h>

#include "Constants.h"

using namespace std;

class Node
{
 public:
  Node ();
  Node (const int& index, const string& type, const Ogre::Vector3& position);
  Node (const Node& node);
  ~Node ();

  int getIndex () const { return _index; }
  string getType () const { return _type; }
  Ogre::Vector3 getPosition () const { return _position; }
  bool getVisitado () const { return _visitado; }
  TipoNodoBola getTipoBola () const { return _tipoBola; }
  int getIdBola () const { return _idBola; }
  operator std::string() const;

  void setVisitado (bool visitado) { _visitado = visitado; }
  void setTipoBola (TipoNodoBola tipoBola) { _tipoBola = tipoBola; }
  void setIdBola (int id) { _idBola = id; }

 private:
  // Índice del nodo (id único).
  int _index;
  // Tipo: generador (spawn), sumidero (drain)
  string _type;
  // Posición del nodo en el espacio 3D.
  Ogre::Vector3 _position;
  /* variables usadas para eliminar la bola y saber que tipo de bola tiene */
  bool _visitado;
  TipoNodoBola _tipoBola;
  int _idBola;
};

#endif
