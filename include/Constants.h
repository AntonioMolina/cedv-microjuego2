#ifndef Constants_H
#define Constants_H

#define MAX_ERROR        0.1
#define N_FANTASMAS      4
#define STR_FANTASMAS    "Fantasma_"
#define N_LIFES          3
#define VELOCIDAD_PACMAN 2
#define VELOCIDAD_GHOSTS 2

#define PUNT_BOLA_PEQUE  15
#define PUNT_BOLA_GRANDE 50
#define PUNT_MUERTE      -50
#define PUNT_COMER       70

#define BOLA_PEQUE       "BolaPeque_"
#define BOLA_GRANDE      "BolaGrande_"

#define INVENCIBLE_TIME  7

enum Sentido {
    stop = 0,
    up = 1,
    down = 2,
    l = 3,
    r = 4
};

enum PosicionCamara {
    inferior = 0,
    derecha = 1,
    superior = 2
};

enum TipoNodoBola {
    noBola = 0,
    bolaPeque = 1,
    bolaGrande = 2
};

#endif /* Constants_H */
