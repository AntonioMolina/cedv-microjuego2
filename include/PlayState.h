#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "Constants.h"
#include "GameState.h"
#include "Game.h"
#include "SoundFX.h"

class PlayState : public Ogre::Singleton<PlayState>, public GameState {
    public:
        PlayState () {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        /* Heredados de Ogre::Singleton. */
        static PlayState& getSingleton ();
        static PlayState* getSingletonPtr ();

    protected:
        CEGUI::Window* _sheet;

        Ogre::Real _deltaT;
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;

    private:
        Game* _game;
        Importer* _importer;
        Sentido _sentidoActual;
        Sentido _sentidoSiguiente;

        PosicionCamara _posCamara;

        bool _invencible;
        Ogre::Real _invencibleT;

        void movePlayer ();
        void moveGhosts ();

        void initPacManAnimation ();
        void updatePacManAnimation ();

        void initBallAnimations ();
        void updateBallAnimations ();

        void createGUIInfo ();
        void deleteGUIInfo ();
        void updateGUIInfo ();

        SoundFXPtr _effectPlayerDie;
        SoundFXPtr _effectKillGhost;
        SoundFXPtr _effectTakeBall;
};

#endif /* PlayState_H */
