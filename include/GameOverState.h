#ifndef GameOverState_H
#define GameOverState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"

class GameOverState : public Ogre::Singleton<GameOverState>, public GameState {
    public:
        GameOverState () {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        /* Heredados de Ogre::Singleton. */
        static GameOverState& getSingleton ();
        static GameOverState* getSingletonPtr ();

        void setGanado (bool ganado) { _ganado = ganado; }

    protected:
        CEGUI::Window* _sheet;

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _goMenu;
        bool _playAgain;
        bool _ganado;

    private:
        void createGUI ();
        void deleteGUI ();

        bool PlayAgain (const CEGUI::EventArgs &e);
        bool GoMenu (const CEGUI::EventArgs &e);
};

#endif /* GameOverState_H */
