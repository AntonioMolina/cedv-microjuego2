#ifndef Utils_h
#define Utils_h

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

class Utils : public Ogre::Singleton<Utils> {
    public:
        Utils () {}

        void createBackground();
        void deleteBackground();

        CEGUI::MouseButton convertMouseButton (OIS::MouseButtonID id);
        Ogre::Vector3 convertAxisBlendToOgre (Ogre::Vector3 v);

        /* Heredados de Ogre::Singleton */
        static Utils& getSingleton ();
        static Utils* getSingletonPtr ();

    private:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
};

#endif
