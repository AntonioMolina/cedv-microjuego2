#ifndef RecordsState_H
#define RecordsState_H

#include <CEGUI.h>
#include <fstream>
#include <iostream>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>
#include <sstream>

#include "GameState.h"

class RecordsState : public Ogre::Singleton<RecordsState>, public GameState {
    public:
        RecordsState () {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

		void saveRecords (int puntuation);
        void createRecordsFile ();
        std::string createStringRecords ();

        /* Heredados de Ogre::Singleton. */
        static RecordsState& getSingleton ();
        static RecordsState* getSingletonPtr ();

    protected:
        CEGUI::Window* _sheet;

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;

    private:
        void createGUI ();
        void deleteGUI ();

        std::vector<std::string> split (std::string str, char delimiter);

        bool back (const CEGUI::EventArgs &e);
};

#endif /* RecordsState_H */
