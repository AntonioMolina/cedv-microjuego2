#ifndef ControlsState_H
#define ControlsState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"

class ControlsState : public Ogre::Singleton<ControlsState>, public GameState {
    public:
        ControlsState () {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        /* Heredados de Ogre::Singleton. */
        static ControlsState& getSingleton ();
        static ControlsState* getSingletonPtr ();

    protected:
        CEGUI::Window* _sheet;

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;

    private:
        void createGUI ();
        void deleteGUI ();

        bool back (const CEGUI::EventArgs &e);
};

#endif /* ControlsState_H */
