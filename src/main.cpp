#define UNUSED_VARIABLE(x) (void)x

#include <iostream>

#include "GameManager.h"

#include "ControlsState.h"
#include "CreditsState.h"
#include "GameOverState.h"
#include "IntroState.h"
#include "PauseState.h"
#include "PlayState.h"
#include "RecordsState.h"
#include "Utils.h"

#include "Importer.h"

using namespace std;

int main () {
    GameManager* game = new GameManager();

    ControlsState* controlsState = new ControlsState();
    CreditsState* creditsState = new CreditsState();
    GameOverState* gameOverState = new GameOverState();
    IntroState* introState = new IntroState();
    PauseState* pauseState = new PauseState();
    PlayState* playState = new PlayState();
    RecordsState* recordsState = new RecordsState();

    Utils* utils = new Utils();

    Importer* importer = new Importer;

    UNUSED_VARIABLE(controlsState);
    UNUSED_VARIABLE(creditsState);
    UNUSED_VARIABLE(gameOverState);
    UNUSED_VARIABLE(introState);
    UNUSED_VARIABLE(pauseState);
    UNUSED_VARIABLE(playState);
    UNUSED_VARIABLE(recordsState);
    UNUSED_VARIABLE(utils);
    UNUSED_VARIABLE(importer);

    srand(time(NULL));
    
    try {
        game->start(IntroState::getSingletonPtr());
    } catch (Ogre::Exception& e) {
        std::cerr << "Excepción detectada: " << e.getFullDescription();
    }

    delete game;

    return 0;
}
