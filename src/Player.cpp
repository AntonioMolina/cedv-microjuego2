#include "Player.h"

Player::Player (Node nodeInicio) {
    _lifes = N_LIFES;
    _nodeActual = nodeInicio;
    _sentActual = stop;
    _miraA = up;
}

Player::~Player () {
}

void Player::die () {
    _lifes--;
}
