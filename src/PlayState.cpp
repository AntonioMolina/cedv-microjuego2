#include <CEGUI.h>
#include <stdio.h>

#include "GameOverState.h"
#include "Ghost.h"
#include "PlayState.h"
#include "PauseState.h"
#include "Scene.h"
#include "Node.h"
#include "RecordsState.h"
#include "Utils.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

void PlayState::enter () {
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _camera->setPosition(Ogre::Vector3(15, 15, 0));
    _camera->lookAt(Ogre::Vector3(0, 0, 0));
    _posCamara = derecha;
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    /* Se recupera el importer */
    _importer = Importer::getSingletonPtr();

    _game = new Game();

    /* Se inician animaciones */
    initPacManAnimation();
    initBallAnimations();

    /* Se oculta el ratón */
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();

    _exitGame = false;
    _sentidoActual = stop;

    _invencible = false;
    _invencibleT = 0;

    _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
    createGUIInfo();

    _effectPlayerDie = GameManager::getSingletonPtr()->getSoundFXManagerPtr()->load("Die.wav");
    _effectKillGhost = GameManager::getSingletonPtr()->getSoundFXManagerPtr()->load("Kill_Ghost.wav");
    _effectTakeBall = GameManager::getSingletonPtr()->getSoundFXManagerPtr()->load("Pick_Ball.wav");

    GameManager::getSingletonPtr()->getTrackMenu()->stop();
    GameManager::getSingletonPtr()->getTrackGame()->play();
}

void PlayState::exit () {
    deleteGUIInfo();
    delete _game;
    /* Se muestra el ratón */
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();

    /* No se elimina la escena, se deja para el gameOver */
    _root->getAutoCreatedWindow()->removeAllViewports();

    GameManager::getSingletonPtr()->getTrackGame()->stop();
    GameManager::getSingletonPtr()->getTrackMenu()->play();
}

void PlayState::pause() {
    /* Se muestra el ratón */
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
}

void PlayState::resume() {
    /* Se oculta el ratón */
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();
}

bool PlayState::frameStarted (const Ogre::FrameEvent& evt) {
    _deltaT = evt.timeSinceLastFrame;

    if (_invencible) {
        _invencibleT -= _deltaT;
        if (_invencibleT <= 0) {
            _invencibleT = 0;
            _invencible = false;
            _game->changeToOriginalColor();
        }
    }

    movePlayer();

    switch (_game->getPlayer()->getSentidoActual()) {
        case up:
            _game->movePlayer(Ogre::Vector3(0, 0, -VELOCIDAD_PACMAN) * _deltaT);
            break;
        case down:
            _game->movePlayer(Ogre::Vector3(0, 0, VELOCIDAD_PACMAN) * _deltaT);
            break;
        case l:
            _game->movePlayer(Ogre::Vector3(-VELOCIDAD_PACMAN, 0, 0) * _deltaT);
            break;
        case r:
            _game->movePlayer(Ogre::Vector3(VELOCIDAD_PACMAN, 0, 0) * _deltaT);
            break;
        default:
        break;
    }

    moveGhosts();

    if (_game->checkColission()) {
        if (_invencible) {
            _effectKillGhost->play();
            _game->addPuntuation(PUNT_COMER);
        } else {
            _effectPlayerDie->play();
            _game->playerDie();
        }
    }

    /* Se actualizan las animaciones del pacman y de las bolas */
    updatePacManAnimation();
    updateBallAnimations();

    return true;
}

bool PlayState::frameEnded (const Ogre::FrameEvent& evt) {
    updateGUIInfo();

    if (_game->checkWin() || _game->checkLose()) {
        if (_game->checkWin()) {
            GameOverState::getSingletonPtr()->setGanado(true);
        } else if (_game->checkLose()) {
            GameOverState::getSingletonPtr()->setGanado(false);
        }

        RecordsState::getSingletonPtr()->saveRecords(_game->getPuntuation());

        changeState(GameOverState::getSingletonPtr());
    }

    return true;
}

void PlayState::keyPressed (const OIS::KeyEvent &e) {
    /* Tecla p --> PauseState. */
    if (e.key == OIS::KC_P || e.key == OIS::KC_ESCAPE) {
        pushState(PauseState::getSingletonPtr());
    } else if (e.key == OIS::KC_UP || e.key == OIS::KC_W) {
        switch (_posCamara) {
            case inferior:
                _game->getPlayer()->setSentSiguiente(up);
                break;
            case derecha:
                _game->getPlayer()->setSentSiguiente(l);
                break;
            case superior:
                break;
        }
    } else if (e.key == OIS::KC_DOWN || e.key == OIS::KC_S) {
        switch (_posCamara) {
            case inferior:
                _game->getPlayer()->setSentSiguiente(down);
                break;
            case derecha:
                _game->getPlayer()->setSentSiguiente(r);
                break;
            case superior:
                break;
        }
    } else if (e.key == OIS::KC_RIGHT || e.key == OIS::KC_D) {
        switch (_posCamara) {
            case inferior:
                _game->getPlayer()->setSentSiguiente(r);
                break;
            case derecha:
                _game->getPlayer()->setSentSiguiente(up);
                break;
            case superior:
                break;
        }
    } else if (e.key == OIS::KC_LEFT || e.key == OIS::KC_A) {
        switch (_posCamara) {
            case inferior:
                _game->getPlayer()->setSentSiguiente(l);
                break;
            case derecha:
                _game->getPlayer()->setSentSiguiente(down);
                break;
            case superior:
                break;
        }
    } else if (e.key == OIS::KC_1 || e.key == OIS::KC_NUMPAD1) {
        _camera->setPosition(Ogre::Vector3(15, 15, 0));
        _camera->lookAt(Ogre::Vector3(0, 0, 0));
        _posCamara = derecha;
    } else if (e.key == OIS::KC_2 || e.key == OIS::KC_NUMPAD2) {
        _camera->setPosition(Ogre::Vector3(7, 30, 7));
        _camera->lookAt(Ogre::Vector3(0, 0, 0));
        _posCamara = inferior;
    } else if (e.key == OIS::KC_3 || e.key == OIS::KC_NUMPAD3) {
        _camera->setPosition(Ogre::Vector3(0, 17, 17));
        _camera->lookAt(Ogre::Vector3(0, 0, 0));
        _posCamara = inferior;
    }
}

void PlayState::keyReleased (const OIS::KeyEvent &e) {
}

void PlayState::mouseMoved (const OIS::MouseEvent &e) {
}

void PlayState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

void PlayState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

PlayState* PlayState::getSingletonPtr () {
    return msSingleton;
}

PlayState& PlayState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void PlayState::movePlayer () {
    Sentido sentActual = _game->getPlayer()->getSentidoActual();

    Sentido sentSiguiente = _game->getPlayer()->getSentidoSiguiente();

    Node nAct = _game->getPlayer()->getNodeActual();
    Node nSig = _game->getPlayer()->getNodeSiguiente();

    if (sentActual == stop) { /* Se presopune que se encuentra quieto en el NodoActual */
        if (sentSiguiente != stop) {
            std::vector<Sentido> vSentidosPosibles = _game->getPossibleSentidos(nAct);

            if (std::find(vSentidosPosibles.begin(), vSentidosPosibles.end(),
                    sentSiguiente) != vSentidosPosibles.end()) {
                _game->getPlayer()->setSentActual(sentSiguiente);
                _game->getPlayer()->setSentSiguiente(stop);

                Scene* scene = _game->getScene();

                GraphVertex* vAct = scene->getGraph()->getVertex(nAct.getIndex());
                std::vector<GraphVertex*> vAdjacents = vAct->adjacents();

                Ogre::Vector3 vAdjAux;
                Ogre::Vector3 vActAux = Utils::getSingletonPtr()->
                    convertAxisBlendToOgre(nAct.getPosition());
                for (unsigned int j = 0; j < vAdjacents.size(); j++) {
                    if (vAdjacents.at(j)->getData().getType().compare("drain") != 0) {
                        vAdjAux = Utils::getSingletonPtr()->
                            convertAxisBlendToOgre(vAdjacents.at(j)->getData().getPosition());

                        if ((_game->getPlayer()->getSentidoActual() == l && vAdjAux.x < vActAux.x
                                    && abs(vActAux.x - vAdjAux.x) > MAX_ERROR) ||
                                (_game->getPlayer()->getSentidoActual() == r && vAdjAux.x > vActAux.x
                                    && abs(vAdjAux.x - vActAux.x) > MAX_ERROR) ||
                                (_game->getPlayer()->getSentidoActual() == up && vAdjAux.z < vActAux.z
                                    && abs(vActAux.z - vAdjAux.z) > MAX_ERROR) ||
                                (_game->getPlayer()->getSentidoActual() == down && vAdjAux.z > vActAux.z
                                    && abs(vAdjAux.z - vActAux.z) > MAX_ERROR)) {
                            _game->getPlayer()->setNodeSiguiente(vAdjacents.at(j)->getData());
                            break;
                        }
                    }
                }
            }
        }
    } else { /* Si se encuentra en movimiento */
        /* Se debe de comprobar si ha llegado al nodo al que iba */
        Ogre::Vector3 vPosActualNode = _sceneMgr->getSceneNode("PacManNode")->getPosition();
        Ogre::Vector3 nSigAux = Utils::getSingletonPtr()->convertAxisBlendToOgre(nSig.getPosition());

        bool llegado = false;
        switch (_game->getPlayer()->getSentidoActual()) {
            case up:
                llegado = vPosActualNode.z < nSigAux.z;
                break;
            case down:
                llegado = vPosActualNode.z > nSigAux.z;
                break;
            case l:
                llegado = vPosActualNode.x < nSigAux.x;
                break;
            case r:
                llegado = vPosActualNode.x > nSigAux.x;
                break;
            default:
                break;
        }

        if (llegado) { /* Si se ha llegado al siguiente nodo */
            /* Se actualiza el nodo actual al nodo al que se ha llegado
               Se traslada a su posicion el pacman para evitar que salga del camino */

            _game->getPlayer()->setNodeActual(_game->getPlayer()->getNodeSiguiente());
            _sceneMgr->getSceneNode("PacManNode")->setPosition
                (Utils::getSingletonPtr()->convertAxisBlendToOgre(_game->getPlayer()->getNodeSiguiente().getPosition()));
            /* Se eleva para que no toque el suelo */
            _sceneMgr->getSceneNode("PacManNode")->translate(Ogre::Vector3(0, 0.3, 0));

            /* Se comprueba si se visito antes o no para eliminar la entidad de la bola */
            if (!_game->getPlayer()->getNodeSiguiente().getVisitado()) {
                GraphVertex* gv = _game->getScene()->getGraph()->getVertex(_game->getPlayer()->getNodeSiguiente().getIndex());
                Node n = gv->getData();
                n.setVisitado(true);
                gv->setData(n);

                TipoNodoBola tipoBola = _game->getPlayer()->getNodeActual().getTipoBola();

                if (tipoBola != noBola) {
                    int idBola = _game->getPlayer()->getNodeActual().getIdBola();
                    std::stringstream nombreBola;

                    try {
                        if (tipoBola == bolaPeque) {
                            nombreBola << BOLA_PEQUE << idBola;
                        } else if (tipoBola == bolaGrande) {
                            nombreBola << BOLA_GRANDE << idBola;
                        }

                        Ogre::Entity* entAux = _sceneMgr->getEntity(nombreBola.str());
                        _sceneMgr->destroyEntity(entAux);

                        if (tipoBola == bolaPeque) {
                            _game->addPuntuation(PUNT_BOLA_PEQUE);
                            _effectTakeBall->play();
                        } else if (tipoBola == bolaGrande) {
                            _effectTakeBall->play();

                            _invencible = true;
                            _invencibleT = INVENCIBLE_TIME;

                            _game->changeColorToBlue();
                        }

                        _game->eatBall();
                    } catch (...) {
                        std::cout << "No se encuentra ya una bola con id " << nombreBola.str() << std::endl;
                    }
                }
            }

            Scene* scene = _game->getScene();
            /* Se comprueba si se puede seguir en el mismo sentido en caso de que
               no se haya pulsado para ir en otro sentido */
            bool seguir = false;
            if (_game->getPlayer()->getSentidoSiguiente() != stop &&
                    _game->getPlayer()->getSentidoSiguiente() !=
                    _game->getPlayer()->getSentidoActual()) {
                std::vector<Sentido> vSentidosPosibles =
                    _game->getPossibleSentidos(_game->getPlayer()->getNodeActual());

                if (std::find(vSentidosPosibles.begin(), vSentidosPosibles.end(),
                        _game->getPlayer()->getSentidoSiguiente()) != vSentidosPosibles.end()) {
                    GraphVertex* vAct = scene->getGraph()->getVertex
                        (_game->getPlayer()->getNodeActual().getIndex());
                    std::vector<GraphVertex*> vAdjacents = vAct->adjacents();

                    bool valid = true;
                    for (unsigned int j = 0; j < vAdjacents.size(); j++) {
                        if (vAdjacents.at(j)->getData().getType().compare("drain") == 0) {
                            valid = false;
                        }
                    }
                    if (valid) {
                        _game->getPlayer()->setSentActual(_game->getPlayer()->
                            getSentidoSiguiente());
                        _game->getPlayer()->setSentSiguiente(stop);
                    } else {
                        seguir = true;
                    }
                } else {
                    seguir = true;
                }
            }

            if (seguir || _game->getPlayer()->getSentidoSiguiente() == stop ||
                    _game->getPlayer()->getSentidoSiguiente() == _game->getPlayer()->getSentidoActual()) {
                std::vector<Sentido> vSentidosPosibles =
                    _game->getPossibleSentidos(_game->getPlayer()->getNodeActual());

                if (std::find(vSentidosPosibles.begin(), vSentidosPosibles.end(),
                        _game->getPlayer()->getSentidoActual()) != vSentidosPosibles.end()) {
                    /* En el estado Actual se mantiene el mismo sentido */
                    if (!seguir) {
                        _game->getPlayer()->setSentSiguiente(stop);
                    }

                    GraphVertex* vAct = scene->getGraph()->getVertex
                        (_game->getPlayer()->getNodeActual().getIndex());
                    std::vector<GraphVertex*> vAdjacents = vAct->adjacents();

                    Ogre::Vector3 vActAux = Utils::getSingletonPtr()->
                        convertAxisBlendToOgre(_game->getPlayer()->getNodeActual().getPosition());
                    Ogre::Vector3 vAdjAux;
                    for (unsigned int j = 0; j < vAdjacents.size(); j++) {
                        if (vAdjacents.at(j)->getData().getType().compare("drain") != 0) {
                            vAdjAux = Utils::getSingletonPtr()->
                                convertAxisBlendToOgre(vAdjacents.at(j)->getData().getPosition());

                            if ((_game->getPlayer()->getSentidoActual() == l && vAdjAux.x < vActAux.x
                                        && abs(vActAux.x - vAdjAux.x) > MAX_ERROR) ||
                                    (_game->getPlayer()->getSentidoActual() == r && vAdjAux.x > vActAux.x
                                        && abs(vAdjAux.x - vActAux.x) > MAX_ERROR) ||
                                    (_game->getPlayer()->getSentidoActual() == up && vAdjAux.z < vActAux.z
                                        && abs(vActAux.z - vAdjAux.z) > MAX_ERROR) ||
                                    (_game->getPlayer()->getSentidoActual() == down && vAdjAux.z > vActAux.z
                                        && abs(vAdjAux.z - vActAux.z) > MAX_ERROR)) {
                                _game->getPlayer()->setNodeSiguiente(vAdjacents.at(j)->getData());
                                break;
                            }
                        }
                    }
                } else {
                    _game->getPlayer()->setSentActual(stop);
                    _game->getPlayer()->setSentSiguiente(stop);
                }
            }

        } else { /* Si todavía está de camino solo se puede cambiar de sentido al sentido inverso */
            Sentido sActual = _game->getPlayer()->getSentidoActual();
            Sentido sSiguiente = _game->getPlayer()->getSentidoSiguiente();
            Node nActual = _game->getPlayer()->getNodeActual();
            Node nSiguiente = _game->getPlayer()->getNodeSiguiente();

            if ((sActual == up && sSiguiente == down) || (sActual == down && sSiguiente == up) ||
                    (sActual == l && sSiguiente == r) || (sActual == r && sSiguiente == l)) {
                Node nAux = nActual;
                _game->getPlayer()->setNodeActual(nSiguiente);
                _game->getPlayer()->setNodeSiguiente(nAux);
                _game->getPlayer()->setSentActual(sSiguiente);
                _game->getPlayer()->setSentSiguiente(stop);
            }
        }
    }

    /* Se reinicia el giro del pacman */
    _sceneMgr->getSceneNode("PacManNode")->resetOrientation();
    switch (_game->getPlayer()->getMiraA()) {
        case up:
            _sceneMgr->getSceneNode("PacManNode")->yaw(Ogre::Degree(-90));
            break;
        case down:
            _sceneMgr->getSceneNode("PacManNode")->yaw(Ogre::Degree(90));
            break;
        case l:
            break;
        case r:
            _sceneMgr->getSceneNode("PacManNode")->yaw(Ogre::Degree(180));
            break;
        default:
            break;
    }
}

void PlayState::moveGhosts () {
    std::stringstream s_fantasma;
    s_fantasma.str("");
    for (int i = 0; i < N_FANTASMAS; i++) {
        Ghost* g = _game->getGhost(i);
        Node nAct = g->getNodeActual();

        s_fantasma << STR_FANTASMAS << i;

        if (g->getSentidoActual() == stop) {
            std::vector<Sentido> vSentidosPosibles = _game->getPossibleSentidos(nAct);
            int posSentidoAleatorio = (int) (rand() % vSentidosPosibles.size());

            g->setSentidoActual(vSentidosPosibles.at(posSentidoAleatorio));
            GraphVertex* vAct = _game->getScene()->getGraph()->getVertex(nAct.getIndex());
            std::vector<GraphVertex*> vAdjacents = vAct->adjacents();

            Ogre::Vector3 vAdjAux;
            Ogre::Vector3 vActAux = Utils::getSingletonPtr()->convertAxisBlendToOgre(nAct.getPosition());

            for (unsigned int j = 0; j < vAdjacents.size(); j++) {
                if (vAdjacents.at(j)->getData().getType().compare("drain") != 0) {
                    vAdjAux = Utils::getSingletonPtr()->convertAxisBlendToOgre(vAdjacents.at(j)->getData().getPosition());
                    if ((g->getSentidoActual() == l && vAdjAux.x < vActAux.x
                                && abs(vActAux.x - vAdjAux.x) > MAX_ERROR) ||
                            (g->getSentidoActual() == r && vAdjAux.x > vActAux.x
                                && abs(vAdjAux.x - vActAux.x) > MAX_ERROR) ||
                            (g->getSentidoActual() == up && vAdjAux.z < vActAux.z
                                && abs(vActAux.z - vAdjAux.z) > MAX_ERROR) ||
                            (g->getSentidoActual() == down && vAdjAux.z > vActAux.z
                                && abs(vAdjAux.z - vActAux.z) > MAX_ERROR)) {
                        g->setNodeSiguiente(vAdjacents.at(j)->getData());
                    }
                }
            }
        } else { /* Si se encuentra en movimiento */
            Ogre::Vector3 vPosActualNode = _sceneMgr->getSceneNode(s_fantasma.str())->getPosition();
            Ogre::Vector3 nSigAux = Utils::getSingletonPtr()->
                            convertAxisBlendToOgre(g->getNodeSiguiente().getPosition());

            bool llegado = false;
            switch (g->getSentidoActual()) {
                case up:
                    llegado = vPosActualNode.z < nSigAux.z;
                    break;
                case down:
                    llegado = vPosActualNode.z > nSigAux.z;
                    break;
                case l:
                    llegado = vPosActualNode.x < nSigAux.x;
                    break;
                case r:
                    llegado = vPosActualNode.x > nSigAux.x;
                    break;
                default:
                    break;
            }

            if (llegado) {
                g->setNodeActual(g->getNodeSiguiente());
                _sceneMgr->getSceneNode(s_fantasma.str())->setPosition
                    (Utils::getSingletonPtr()->convertAxisBlendToOgre(g->getNodeSiguiente().getPosition()));
                _sceneMgr->getSceneNode(s_fantasma.str())->translate(Ogre::Vector3(0, 0.3, 0));

                std::vector<Sentido> vSentidosPosibles = _game->getPossibleSentidos(g->getNodeActual());
                int posSentidoAleatorio = (int) (rand() % vSentidosPosibles.size());

                g->setSentidoActual(vSentidosPosibles.at(posSentidoAleatorio));
                GraphVertex* vAct = _game->getScene()->getGraph()->getVertex(g->getNodeActual().getIndex());
                std::vector<GraphVertex*> vAdjacents = vAct->adjacents();

                Ogre::Vector3 vAdjAux;
                Ogre::Vector3 vActAux = Utils::getSingletonPtr()->convertAxisBlendToOgre(g->getNodeActual().getPosition());
                for (unsigned int j = 0; j < vAdjacents.size(); j++) {
                    if (vAdjacents.at(j)->getData().getType().compare("drain") != 0) {
                        vAdjAux = Utils::getSingletonPtr()->convertAxisBlendToOgre(vAdjacents.at(j)->getData().getPosition());
                        if ((g->getSentidoActual() == l && vAdjAux.x < vActAux.x
                                    && abs(vActAux.x - vAdjAux.x) > MAX_ERROR) ||
                                (g->getSentidoActual() == r && vAdjAux.x > vActAux.x
                                    && abs(vAdjAux.x - vActAux.x) > MAX_ERROR) ||
                                (g->getSentidoActual() == up && vAdjAux.z < vActAux.z
                                    && abs(vActAux.z - vAdjAux.z) > MAX_ERROR) ||
                                (g->getSentidoActual() == down && vAdjAux.z > vActAux.z
                                    && abs(vAdjAux.z - vActAux.z) > MAX_ERROR)) {
                            g->setNodeSiguiente(vAdjacents.at(j)->getData());
                        }
                    }
                }
            }
        }

        _sceneMgr->getSceneNode(s_fantasma.str())->resetOrientation();
        _sceneMgr->getSceneNode(s_fantasma.str())->yaw(Ogre::Degree(-30));
        switch (g->getSentidoActual()) {
            case up:
                _sceneMgr->getSceneNode(s_fantasma.str())->yaw(Ogre::Degree(180));
                break;
            case down:
                break;
            case l:
                _sceneMgr->getSceneNode(s_fantasma.str())->yaw(Ogre::Degree(-90));
                break;
            case r:
                _sceneMgr->getSceneNode(s_fantasma.str())->yaw(Ogre::Degree(90));
                break;
            default:
                break;
        }

        switch (g->getSentidoActual()) {
            case up:
                _game->moveGhost(Ogre::Vector3(0, 0, -VELOCIDAD_GHOSTS) * _deltaT, i);
                break;
            case down:
                _game->moveGhost(Ogre::Vector3(0, 0, VELOCIDAD_GHOSTS) * _deltaT, i);
                break;
            case l:
                _game->moveGhost(Ogre::Vector3(-VELOCIDAD_GHOSTS, 0, 0) * _deltaT, i);
                break;
            case r:
                _game->moveGhost(Ogre::Vector3(VELOCIDAD_GHOSTS, 0, 0) * _deltaT, i);
                break;
            default:
            break;
        }

        if (_invencible && _game->checkColission(i)) {
            g->setSentidoActual(stop);

            Ogre::Vector3 vPosDrain;
            std::vector<GraphVertex*> vertexesGraph = _game->getScene()->getGraph()->getVertexes();
            Node n;
            for (unsigned int i = 0; i < vertexesGraph.size(); i++) {
                n = vertexesGraph.at(i)->getData();
                string type = n.getType();
                if (type.compare("drain") == 0) {
                    vPosDrain = n.getPosition();
                    g->setNodeActual(n);

                    Ogre::SceneNode* nodeF = _sceneMgr->getSceneNode(s_fantasma.str());
                    nodeF->setPosition(Utils::getSingletonPtr()->convertAxisBlendToOgre(vPosDrain));

                    break;
                }
            }
        }

        s_fantasma.str("");
    }
}

void PlayState::initPacManAnimation () {
    try {
        Ogre::AnimationState* animState = _sceneMgr->getEntity("Pacman")->getAnimationState("moveMouth");
        animState->setEnabled(true);
        animState->setLoop(true);
        animState->setTimePosition(0.0);
    } catch (...) {
        std::cout << "No se puede iniciar la animacion del Pacman" << std::endl;
    }
}

void PlayState::updatePacManAnimation () {
    try {
        Ogre::AnimationState* animState = _sceneMgr->getEntity("Pacman")->getAnimationState("moveMouth");

        if (animState->hasEnded()) {
            animState->setTimePosition(0.0);
            animState->setEnabled(false);
        } else {
            animState->addTime(_deltaT);
        }
    } catch (...) {
        std::cout << "No se puede actualizar la animacion del Pacman" << std::endl;
    }
}

void PlayState::initBallAnimations () {
    /* Se obtienen todos los vértices dónde van las bolas grandes */
    std::vector<GraphVertex*> vertexes = _game->getScene()->getGraph()->getVertexes();

    int nBolasGrandes = 0;
    for (unsigned int i = 0; i < vertexes.size(); i++) {
        Node n = vertexes.at(i)->getData();
        string type = n.getType();

        if (type.compare("BolaGrande") == 0) {
            nBolasGrandes++;
            std::stringstream nombreBola;
            nombreBola << "BolaGrande_" << nBolasGrandes;

            try {
                Ogre::AnimationState* animState = _sceneMgr->getEntity(nombreBola.str())->getAnimationState("Bote");
                animState->setEnabled(true);
                animState->setLoop(true);
                animState->setTimePosition(0.0);
            } catch (...) {
                std::cout << "No se puede iniciar la animacion de la bola " << nBolasGrandes << std::endl;
            }

        }
    }
}

void PlayState::updateBallAnimations () {
    /* Se obtienen todos los vértices dónde van las bolas grandes */
    std::vector<GraphVertex*> vertexes = _game->getScene()->getGraph()->getVertexes();

    int nBolasGrandes = 0;
    for (unsigned int i = 0; i < vertexes.size(); i++) {
        Node n = vertexes.at(i)->getData();
        string type = n.getType();

        if (type.compare("BolaGrande") == 0) {
            nBolasGrandes++;
            std::stringstream nombreBola;
            nombreBola << "BolaGrande_" << nBolasGrandes;

            try {
                Ogre::AnimationState* animState = _sceneMgr->getEntity(nombreBola.str())->getAnimationState("Bote");

                if (animState->hasEnded()) {
                    animState->setTimePosition(0.0);
                    animState->setEnabled(false);
                } else {
                    animState->addTime(_deltaT);
                }
            } catch (...) {
                std::cout << "No se puede actualizar la animacion de la bola " << nBolasGrandes << std::endl;
            }
        }
    }
}

void PlayState::createGUIInfo () {
    CEGUI::Window* lifes = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "LifesLabel");
    lifes->setSize(CEGUI::USize(CEGUI::UDim(0.2,0),CEGUI::UDim(0.1,0)));
    lifes->setPosition(CEGUI::UVector2(CEGUI::UDim(0.05,0),CEGUI::UDim(0.05,0)));
    _sheet->addChild(lifes);

    CEGUI::Window* puntuation = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "PuntuationLabel");
    puntuation->setSize(CEGUI::USize(CEGUI::UDim(0.3,0),CEGUI::UDim(0.1,0)));
    puntuation->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0),CEGUI::UDim(0.05,0)));
    _sheet->addChild(puntuation);
}

void PlayState::deleteGUIInfo () {
    _sheet->destroyChild("LifesLabel");
    _sheet->destroyChild("PuntuationLabel");
}

void PlayState::updateGUIInfo () {
    CEGUI::Window* lifes = _sheet->getChild("LifesLabel");
    std::stringstream s_lifes;
    for (int i = 0; i < _game->getLifes(); i++) {
        s_lifes << 1 << " ";
    }
    lifes->setText("[colour='FFFFFFFF']Vidas: [font='PacFont-Lifes']" + s_lifes.str());

    CEGUI::Window* puntuation = _sheet->getChild("PuntuationLabel");
    std::stringstream s_Punts;
    s_Punts << "[colour='FFFFFFFF']Puntuacion: " << _game->getPuntuation();
    puntuation->setText(s_Punts.str());
}
