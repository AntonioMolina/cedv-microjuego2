#include "GameOverState.h"
#include "IntroState.h"
#include "PlayState.h"
#include "Utils.h"

template<> GameOverState* Ogre::Singleton<GameOverState>::msSingleton = 0;

void GameOverState::enter () {
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();

    createGUI();

    _playAgain = false;
    _goMenu = false;
}

void GameOverState::exit () {
    deleteGUI();

    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void GameOverState::pause() {
}

void GameOverState::resume() {

}

bool GameOverState::frameStarted (const Ogre::FrameEvent& evt) {
    return true;
}

bool GameOverState::frameEnded (const Ogre::FrameEvent& evt) {
    if (_playAgain) {
        changeState(PlayState::getSingletonPtr());
    } else if (_goMenu) {
        changeState(IntroState::getSingletonPtr());
    }

    return true;
}

void GameOverState::keyPressed (const OIS::KeyEvent &e) {
    /* Tecla space --> PlayState. */
    if (e.key == OIS::KC_SPACE) {
        _playAgain = true;
    }
}

void GameOverState::keyReleased (const OIS::KeyEvent &e) {
    /* Tecla escape --> IntroState. */
    if (e.key == OIS::KC_ESCAPE) {
        _goMenu = true;
    }
}

void GameOverState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void GameOverState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(Utils::getSingletonPtr()->convertMouseButton(id));
}

void GameOverState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(Utils::getSingletonPtr()->convertMouseButton(id));
}

GameOverState* GameOverState::getSingletonPtr () {
    return msSingleton;
}

GameOverState& GameOverState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void GameOverState::createGUI () {
    /* Texto de Pausa */
    CEGUI::Window* gameOverLabel = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "GameOverLabel");
    if (_ganado) {
        gameOverLabel->setText("[colour='FFFFFFFF'][font='PacFont-Title']1 1[font='DejaVuSans-40']Enhorabuena\nHas Ganado!  [font='PacFont-Title']1 1");
    } else {
        gameOverLabel->setText("[colour='FFFFFFFF'][font='DejaVuSans-40']Has Perdido!\n [font='PacFont-Title']9 9 9 9");
    }
    gameOverLabel->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.7,0)));
    gameOverLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.7/2,0),CEGUI::UDim(0.4-0.7/2,0)));
    _sheet->addChild(gameOverLabel);

    /* Botón volver menú */
    CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "GoMenuButton");
    backButton->setText("Ir al Menu");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.6-0.15/2,0),CEGUI::UDim(0.55,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GameOverState::GoMenu, this));
    _sheet->addChild(backButton);

    /* Botón jugar de nuevo */
    CEGUI::Window* playAgainButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "PlayAgainButton");
    playAgainButton->setText("Jugar de Nuevo");
    playAgainButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    playAgainButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4-0.15/2,0),CEGUI::UDim(0.55,0)));
    playAgainButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GameOverState::PlayAgain, this));
    _sheet->addChild(playAgainButton);
}

void GameOverState::deleteGUI () {
    _sheet->destroyChild("GameOverLabel");
    _sheet->destroyChild("GoMenuButton");
    _sheet->destroyChild("PlayAgainButton");
}

bool GameOverState::PlayAgain (const CEGUI::EventArgs &e) {
    _playAgain = true;
    return true;
}

bool GameOverState::GoMenu (const CEGUI::EventArgs &e) {
    _goMenu = true;
    return true;
}
