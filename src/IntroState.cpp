#include "IntroState.h"

#include "ControlsState.h"
#include "CreditsState.h"
#include "PlayState.h"
#include "RecordsState.h"
#include "Utils.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

IntroState::IntroState () {
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
    _sceneMgr->setAmbientLight(Ogre::ColourValue(1, 1, 1));

    _camera = _sceneMgr->createCamera("MainCamera");
    _camera->setPosition(Ogre::Vector3(7, 30, 7));
    _camera->lookAt(Ogre::Vector3(0, 0, 0));
    _camera->setNearClipDistance(0.1);
    _camera->setFarClipDistance(1000);

    setCEGUI();
}

void IntroState::enter () {
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();

    Utils::getSingletonPtr()->createBackground();
    createGUI();

    _exitGame = false;
}

void IntroState::exit() {
    deleteGUI();
    Utils::getSingletonPtr()->deleteBackground();
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void IntroState::pause () {
}

void IntroState::resume () {
}

bool IntroState::frameStarted (const Ogre::FrameEvent& evt) {
    return true;
}

bool IntroState::frameEnded (const Ogre::FrameEvent& evt) {
    if (_exitGame)
        //popState();
        return false;

    return true;
}

void IntroState::keyPressed (const OIS::KeyEvent &e) {
    /* Transición al siguiente estado.
       Espacio --> PlayState
       C -> CreditsState
       R -> RecordsState
       H -> ControlsState */
    if (e.key == OIS::KC_SPACE) {
        changeState(PlayState::getSingletonPtr());
    } else if (e.key == OIS::KC_C) {
        changeState(CreditsState::getSingletonPtr());
    } else if (e.key == OIS::KC_R) { /*  */
        changeState(RecordsState::getSingletonPtr());
    } else if (e.key == OIS::KC_H) { /*  */
        changeState(ControlsState::getSingletonPtr());
    }
}

void IntroState::keyReleased (const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void IntroState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void IntroState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(Utils::getSingletonPtr()->convertMouseButton(id));
}

void IntroState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(Utils::getSingletonPtr()->convertMouseButton(id));
}

IntroState* IntroState::getSingletonPtr () {
    return msSingleton;
}

IntroState& IntroState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void IntroState::setCEGUI () {
    _renderer = &CEGUI::OgreRenderer::bootstrapSystem();
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

    CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
    CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");

    CEGUI::FontManager::getSingleton().createAll("*.font", "Fonts");

    _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","PacMan");

    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_sheet);
}

void IntroState::createGUI () {
    /* Titulo */
    CEGUI::Window* titulo = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "Gametitle");
    titulo->setText("[colour='FFFFFFFF'][font='PacFont-Title']1 Pac Man 1");
    titulo->setSize(CEGUI::USize(CEGUI::UDim(0.4,0),CEGUI::UDim(0.1,0)));
    titulo->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.4/2,0),CEGUI::UDim(0.2,0)));
    _sheet->addChild(titulo);

    /* Play button */
    CEGUI::Window* playButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "PlayButton");
    playButton->setText("Jugar");
    playButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    playButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.4,0)));
    playButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::start, this));
    _sheet->addChild(playButton);

    /* Controls button */
    CEGUI::Window* controlsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "ControlsButton");
    controlsButton->setText("Controles");
    controlsButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    controlsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.5,0)));
    controlsButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::controls, this));
    _sheet->addChild(controlsButton);

    /* Records button */
    CEGUI::Window* recordsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "RecordsButton");
    recordsButton->setText("Records");
    recordsButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    recordsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.6,0)));
    recordsButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::records, this));
    _sheet->addChild(recordsButton);

    /* Creditos button */
    CEGUI::Window* creditsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "CreditsButton");
    creditsButton->setText("Creditos");
    creditsButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    creditsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.7,0)));
    creditsButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::credits, this));
    _sheet->addChild(creditsButton);

    /* Exit button */
    CEGUI::Window* exitButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "ExitButton");
    exitButton->setText("Salir");
    exitButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    exitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.8,0)));
    exitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&IntroState::quit, this));
    _sheet->addChild(exitButton);
}

void IntroState::deleteGUI () {
    _sheet->destroyChild("Gametitle");
    _sheet->destroyChild("PlayButton");
    _sheet->destroyChild("ControlsButton");
    _sheet->destroyChild("RecordsButton");
    _sheet->destroyChild("CreditsButton");
    _sheet->destroyChild("ExitButton");
}

bool IntroState::start (const CEGUI::EventArgs &e) {
    changeState(PlayState::getSingletonPtr());
    return true;
}

bool IntroState::quit (const CEGUI::EventArgs &e) {
    _exitGame = true;
    return true;
}

bool IntroState::credits (const CEGUI::EventArgs &e) {
    changeState(CreditsState::getSingletonPtr());
    return true;
}

bool IntroState::records (const CEGUI::EventArgs &e) {
    changeState(RecordsState::getSingletonPtr());
    return true;
}

bool IntroState::controls (const CEGUI::EventArgs &e) {
    changeState(ControlsState::getSingletonPtr());
    return true;
}
