#include "PauseState.h"

#include "Utils.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void PauseState::enter () {
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    //_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();

    createGUI();
    _backToGame = false;
}

void PauseState::exit () {
    deleteGUI();
}

void PauseState::pause() {
}

void PauseState::resume() {
}

bool PauseState::frameStarted (const Ogre::FrameEvent& evt) {
    return true;
}

bool PauseState::frameEnded (const Ogre::FrameEvent& evt) {
    if (_backToGame) {
        popState();
    }

    return true;
}

void PauseState::keyPressed (const OIS::KeyEvent &e) {
    /* Tecla p o ESCAPE --> PauseState. */
    if (e.key == OIS::KC_P || e.key == OIS::KC_ESCAPE) {
        _backToGame = true;
    }
}

void PauseState::keyReleased (const OIS::KeyEvent &e) {
}

void PauseState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void PauseState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(Utils::getSingletonPtr()->convertMouseButton(id));
}

void PauseState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(Utils::getSingletonPtr()->convertMouseButton(id));
}

PauseState* PauseState::getSingletonPtr () {
    return msSingleton;
}

PauseState& PauseState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void PauseState::createGUI () {
    /* Texto de Pausa */
    CEGUI::Window* pauseLabel = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "PauseLabel");
    pauseLabel->setText("[colour='FFFFFFFF'][font='PacFont-Title']1 0 Pausa 0 9");
    pauseLabel->setSize(CEGUI::USize(CEGUI::UDim(0.4,0),CEGUI::UDim(0.1,0)));
    pauseLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.4/2,0),CEGUI::UDim(0.25,0)));
    _sheet->addChild(pauseLabel);

    /* Botón volver menú */
    CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "BackButton");
    backButton->setText("Volver");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.4,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseState::back, this));
    _sheet->addChild(backButton);
}

void PauseState::deleteGUI () {
    _sheet->destroyChild("PauseLabel");
    _sheet->destroyChild("BackButton");
}

bool PauseState::back (const CEGUI::EventArgs &e) {
    _backToGame = true;
    return true;
}
