#include <iostream>

#include <GraphVertex.h>

#include "Game.h"
#include "Utils.h"

Game::Game () {
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _nTotalBolas = 0;
    _puntuation = 0;
    _ghosts.reserve(N_FANTASMAS);

    try {
        /* Se recupera el importer y se obtiene el grafo */
        _importer = Importer::getSingletonPtr();

        _scene = new Scene;

        _importer->parseScene("./media/XML_levels/Level_1.xml", _scene);
    } catch (...) {
        std::cerr << "Unexpected exception!" << std::endl;
    }
	createBackground();

    /* Se crea el tablero */
    createTable();

    for (unsigned int i = 0; i < _scene->getGraph()->getVertexes().size(); i++) {
        Node aux = _scene->getGraph()->getVertexes().at(i)->getData();
        string type = aux.getType();
        if (type.compare("spawn") == 0) {
            _player = new Player(aux);
            break;
        }
    }

    /* Se crea el pacman */
    createPacman();

    /* Se crean las bolas grandes en los nodos */
	createBolasGrandes();

    /* Se crean las bolas pequeñas en los nodos que no tienen etiquetas */
    createBolasPeques();

    /* Se crean los fantasmas */
    createGhosts();
}

Game::~Game () {
}

int Game::getLifes () {
    return _player->getLifes();
}

std::vector<Sentido> Game::getPossibleSentidos (Node nAct) {
    std::vector<Sentido> vSentidos;

    GraphVertex* vAct = _scene->getGraph()->getVertex(nAct.getIndex());
    std::vector<GraphVertex*> adjacentVertices = vAct->adjacents();

    GraphVertex* vAux;
    for (unsigned int i = 0; i < adjacentVertices.size(); i++) {
        vAux = adjacentVertices.at(i);

        if (nAct.getPosition().x > vAux->getData().getPosition().x &&
                abs(max(nAct.getPosition().x, vAux->getData().getPosition().x) -
                    min(nAct.getPosition().x, vAux->getData().getPosition().x)) > MAX_ERROR) {
            vSentidos.push_back(l);
        } else if (nAct.getPosition().x < vAux->getData().getPosition().x &&
                abs(max(nAct.getPosition().x, vAux->getData().getPosition().x) -
                    min(nAct.getPosition().x, vAux->getData().getPosition().x)) > MAX_ERROR) {
            vSentidos.push_back(r);
        }
        if (nAct.getPosition().y > vAux->getData().getPosition().y &&
                abs(max(nAct.getPosition().y, vAux->getData().getPosition().y) -
                    min(nAct.getPosition().y, vAux->getData().getPosition().y)) > MAX_ERROR) {
            vSentidos.push_back(down);
        } else if (nAct.getPosition().y < vAux->getData().getPosition().y &&
                abs(max(nAct.getPosition().y, vAux->getData().getPosition().y) -
                    min(nAct.getPosition().y, vAux->getData().getPosition().y)) > MAX_ERROR) {
            vSentidos.push_back(up);
        }
    }

    return vSentidos;
}

void Game::eatBall () {
    _nTotalBolas--;
}

bool Game::checkLose () {
    return _player->getLifes() == 0;
}

bool Game::checkWin () {
    return _nTotalBolas <= 0;
}

bool Game::checkColission () {
    Ogre::Entity* entPacMan = _sceneMgr->getEntity("Pacman");

    std::stringstream s_fantasma;
    s_fantasma.str("");
    for (unsigned int i = 0; i < _ghosts.size(); i++) {
        s_fantasma << STR_FANTASMAS << i;
        Ogre::Entity* entFantasmaAux = _sceneMgr->getEntity(s_fantasma.str());

        if (entPacMan->getWorldBoundingBox().intersects(entFantasmaAux->getWorldBoundingBox())) {
            return true;
        }

        s_fantasma.str("");
    }

    return false;
}

bool Game::checkColission (int id) {
    Ogre::Entity* entPacMan = _sceneMgr->getEntity("Pacman");

    std::stringstream s_fantasma;
    s_fantasma.str("");
    s_fantasma << STR_FANTASMAS << id;

    Ogre::Entity* entFantasmaAux = _sceneMgr->getEntity(s_fantasma.str());

    if (entPacMan->getWorldBoundingBox().intersects(entFantasmaAux->getWorldBoundingBox())) {
        return true;
    }

    return false;
}

void Game::createTable () {
    /* Se crea el nodo de Juego y se añade al Root */
    _nodeJuego = _sceneMgr->createSceneNode("Juego");
    _sceneMgr->getRootSceneNode()->addChild(_nodeJuego);

    /* Se crean los nodos de los tableros */
    _nodeTablero = _sceneMgr->createSceneNode("Tablero");
    _nodeJuego->addChild(_nodeTablero);

    /* Se crean las luces */
    createLights();

    /* Se añaden las entidades a los tableros IA y Jugador */
    Ogre::Entity* entityTablero = _sceneMgr->createEntity("Tablero", "Tablero_1.mesh");

    _nodeTablero->attachObject(entityTablero);
}

void Game::createLights () {
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

    _light = _sceneMgr->createLight("Light");
    _light->setType(Ogre::Light::LT_SPOTLIGHT);
    _light->setPosition(0, 25, 0);
    _light->setDirection(Ogre::Vector3(0, 0, 0));
    _light->setSpotlightInnerAngle(Ogre::Degree(5.0f));
    _light->setSpotlightOuterAngle(Ogre::Degree(55.0f));
    _light->setSpotlightFalloff(0.0f);

    _nodeTablero->attachObject(_light);

    Ogre::Light* light2 = _sceneMgr->createLight("Light2");
  	light2->setType(Ogre::Light::LT_POINT);
  	light2->setPosition(8, 8, -2);
  	light2->setSpecularColour(0.9, 0.9, 0.9);
  	light2->setDiffuseColour(0.9, 0.9, 0.9);

	_nodeTablero->attachObject(light2);

	Ogre::Light* light3 = _sceneMgr->createLight("Light3");
  	light3->setType(Ogre::Light::LT_POINT);
  	light3->setPosition(150, 150, -2);
  	light3->setSpecularColour(0.9, 0.9, 0.9);
  	light3->setDiffuseColour(0.9, 0.9, 0.9);

	_nodeTablero->attachObject(light3);
}

void Game::createPacman () {
    _nodePacMan = _sceneMgr->createSceneNode("PacManNode");
    _nodeTablero->addChild(_nodePacMan);

    Ogre::Entity* entityPacMan = _sceneMgr->createEntity("Pacman", "Pacman.mesh");
    _nodePacMan->attachObject(entityPacMan);
    _nodePacMan->scale(0.35, 0.35, 0.35);
    _nodePacMan->yaw(Ogre::Degree(-90));

    respawnPacMan();
}

void Game::createBolasGrandes () {
	Ogre::SceneNode* nodeBolaGrande;

    /* Se obtienen todos los vértices dónde van las bolas grandes */
    std::vector<GraphVertex*> vertexes = _scene->getGraph()->getVertexes();
    int nBolasGrandes = 0;
    for (unsigned int i = 0; i < vertexes.size(); i++) {
        Node n = vertexes.at(i)->getData();
        string type = n.getType();
        if (type.compare("BolaGrande") == 0) {
            nBolasGrandes++;
            _nTotalBolas++;
            std::stringstream nombreBola;
            nombreBola << BOLA_GRANDE << nBolasGrandes;

            nodeBolaGrande = _sceneMgr->createSceneNode(nombreBola.str());
            _nodeTablero->addChild(nodeBolaGrande);

            Ogre::Entity* entityBolaGrande = _sceneMgr->createEntity(nombreBola.str(), "BolaGrande.mesh");
            nodeBolaGrande->attachObject(entityBolaGrande);
            nodeBolaGrande->scale(0.7, 0.7, 0.7);

            nodeBolaGrande->setPosition(Utils::getSingletonPtr()->convertAxisBlendToOgre(n.getPosition()));
            //nodeBolaGrande->translate(Ogre::Vector3(0, 0.2, 0));
            n.setTipoBola(bolaGrande);
            n.setIdBola(nBolasGrandes);
            vertexes.at(i)->setData(n);
        }
    }
}

void Game::createBolasPeques () {
    Ogre::SceneNode* nodeBolaPeque;

    /* Se obtienen todos los vértices (nodos) */
    std::vector<GraphVertex*> vertexes = _scene->getGraph()->getVertexes();
    int nBolasPeques = 0;

    for (unsigned int i = 0; i < vertexes.size(); i++) {
        Node n = vertexes.at(i)->getData();
        string type = n.getType();
        if (type.compare("") == 0) {
            nBolasPeques++;
            _nTotalBolas++;
            std::stringstream nombreBola;
            nombreBola << BOLA_PEQUE << nBolasPeques;

            nodeBolaPeque = _sceneMgr->createSceneNode(nombreBola.str());
            _nodeTablero->addChild(nodeBolaPeque);

            Ogre::Entity* entityBolaPeque = _sceneMgr->createEntity(nombreBola.str(), "BolaPeque.mesh");
            nodeBolaPeque->attachObject(entityBolaPeque);
            nodeBolaPeque->scale(0.35, 0.35, 0.35);

            nodeBolaPeque->setPosition(Utils::getSingletonPtr()->convertAxisBlendToOgre(n.getPosition()));
            nodeBolaPeque->translate(Ogre::Vector3(0, 0.35, 0));
            n.setTipoBola(bolaPeque);
            n.setIdBola(nBolasPeques);
            vertexes.at(i)->setData(n);
        }
    }
}

void Game::createGhosts () {
    /* Se obtiene el nodo Drain */
    Ogre::Vector3 vPosDrain;
    std::vector<GraphVertex*> vertexesGraph = _scene->getGraph()->getVertexes();
    Node n;
    for (unsigned int i = 0; i < vertexesGraph.size(); i++) {
        n = vertexesGraph.at(i)->getData();
        string type = n.getType();
        if (type.compare("drain") == 0) {
            vPosDrain = n.getPosition();
            break;
        }
    }

    std::stringstream s_fantasma;
    s_fantasma.str("");

    for (int i = 0; i < N_FANTASMAS; i++) {
        s_fantasma << STR_FANTASMAS << i;

        Ogre::SceneNode* nFantasmaAux = _sceneMgr->createSceneNode(s_fantasma.str());
        _nodeTablero->addChild(nFantasmaAux);

        Ogre::Entity* entFantasmaAux = _sceneMgr->createEntity(s_fantasma.str(), "fantasma.mesh");

        std::stringstream saux;
        saux << "Material" << (i + 1);
        entFantasmaAux->setMaterialName(saux.str());

        nFantasmaAux->attachObject(entFantasmaAux);
        nFantasmaAux->scale(0.35, 0.35, 0.35);

        nFantasmaAux->setPosition(Utils::getSingletonPtr()->convertAxisBlendToOgre(vPosDrain));
        _ghosts.push_back(new Ghost(n, i));

        s_fantasma.str("");
    }
}

void Game::createBackground () {
	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Background3", "General");
    material->getTechnique(0)->getPass(0)->createTextureUnitState("background_3.jpg");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

    Ogre::Rectangle2D* rect = new Ogre::Rectangle2D(true);
    rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    rect->setMaterial("Background3");

    rect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);

    Ogre::AxisAlignedBox aabInf;
    aabInf.setInfinite();
    rect->setBoundingBox(aabInf);

    Ogre::SceneNode* node = _sceneMgr->getRootSceneNode()->createChildSceneNode("Background3");
    node->attachObject(rect);
}

void Game::deleteBackground () {
    _sceneMgr->destroySceneNode("Background3");
}

void Game::movePlayer (Ogre::Vector3 v) {
    _nodePacMan->translate(v);
}

void Game::moveGhost (Ogre::Vector3 v, int id) {
    std::stringstream s_fantasma;
    s_fantasma.str("");
    s_fantasma << STR_FANTASMAS << id;

    Ogre::SceneNode* nodeFantasma = _sceneMgr->getSceneNode(s_fantasma.str());
    nodeFantasma->translate(v);
}

void Game::playerDie () {
    _player->die();

    addPuntuation(PUNT_MUERTE);
    respawnPacMan();
    respawnGhosts();
}

void Game::respawnPacMan () {
    for (unsigned int i = 0; i < _scene->getGraph()->getVertexes().size(); i++) {
        Node aux = _scene->getGraph()->getVertexes().at(i)->getData();
        string type = aux.getType();
        if (type.compare("spawn") == 0) {
            _nodePacMan->setPosition(Utils::getSingletonPtr()->convertAxisBlendToOgre(aux.getPosition()));
            _nodePacMan->translate(0, 0.3, 0);

            _player->setSentActual(stop);
            _player->setSentSiguiente(stop);
            _player->setNodeActual(aux);
            _player->setNodeSiguiente(aux);
            break;
        }
    }
}

void Game::spawnBola (Ogre::SceneNode* nodeBola, Ogre::Vector3 pos) {
    nodeBola->setPosition(pos);
}

void Game::respawnGhosts () {
    Ogre::Vector3 vPosDrain;
    std::vector<GraphVertex*> vertexesGraph = _scene->getGraph()->getVertexes();
    Node n;
    for (unsigned int i = 0; i < vertexesGraph.size(); i++) {
        n = vertexesGraph.at(i)->getData();
        string type = n.getType();
        if (type.compare("drain") == 0) {
            vPosDrain = n.getPosition();
            break;
        }
    }

    std::stringstream s_fantasma;
    s_fantasma.str("");

    for (int i = 0; i < N_FANTASMAS; i++) {
        s_fantasma << STR_FANTASMAS << i;

        Ogre::SceneNode* nFantasmaAux = _sceneMgr->getSceneNode(s_fantasma.str());
        nFantasmaAux->setPosition(Utils::getSingletonPtr()->convertAxisBlendToOgre(vPosDrain));

        _ghosts.at(i)->setNodeActual(n);
        _ghosts.at(i)->setSentidoActual(stop);

        s_fantasma.str("");
    }
}

void Game::addPuntuation (int punts) {
    _puntuation += punts;
}

void Game::changeColorToBlue () {
    std::stringstream s_fantasma;
    s_fantasma.str("");

    for (int i = 0; i < N_FANTASMAS; i++) {
        s_fantasma << STR_FANTASMAS << i;

        Ogre::Entity* entFantasmaAux = _sceneMgr->getEntity(s_fantasma.str());
        entFantasmaAux->setMaterialName("Material5");

        s_fantasma.str("");
    }
}

void Game::changeToOriginalColor () {
    std::stringstream s_fantasma;
    s_fantasma.str("");

    for (int i = 0; i < N_FANTASMAS; i++) {
        s_fantasma << STR_FANTASMAS << i;

        Ogre::Entity* entFantasmaAux = _sceneMgr->getEntity(s_fantasma.str());

        std::stringstream saux;
        saux << "Material" << (i + 1);
        entFantasmaAux->setMaterialName(saux.str());

        s_fantasma.str("");
    }
}
