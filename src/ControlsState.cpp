#include "ControlsState.h"
#include "IntroState.h"
#include "Utils.h"

template<> ControlsState* Ogre::Singleton<ControlsState>::msSingleton = 0;

void ControlsState::enter () {
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();

    Utils::getSingletonPtr()->createBackground();
    createGUI();

    _exitGame = false;
}

void ControlsState::exit () {
    deleteGUI();
    Utils::getSingletonPtr()->deleteBackground();
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void ControlsState::pause() {
}

void ControlsState::resume() {
}

bool ControlsState::frameStarted (const Ogre::FrameEvent& evt) {
    return true;
}

bool ControlsState::frameEnded (const Ogre::FrameEvent& evt) {
    if (_exitGame)
        changeState(IntroState::getSingletonPtr());

    return true;
}

void ControlsState::keyPressed (const OIS::KeyEvent &e) {
    /* Tecla H --> IntroState. */
    if (e.key == OIS::KC_H) {
        changeState(IntroState::getSingletonPtr());
    }
}

void ControlsState::keyReleased (const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void ControlsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void ControlsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(Utils::getSingletonPtr()->convertMouseButton(id));
}

void ControlsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(Utils::getSingletonPtr()->convertMouseButton(id));
}

ControlsState* ControlsState::getSingletonPtr () {
    return msSingleton;
}

ControlsState& ControlsState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void ControlsState::createGUI () {
    /* Botón volver menú */
    CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "BackButton");
    backButton->setText("Volver");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.7,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ControlsState::back, this));
    _sheet->addChild(backButton);


	CEGUI::Window* controles = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "Controles");
    controles->setText("[colour='FFFFFFFF'][font='DejaVuSans-25']Controles: \nPara moverse: A S D W \nPuede cambiar la vista con: 1, 2, 3");
    controles->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.2,0)));
    controles->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.7/2,0),CEGUI::UDim(0.35,0)));
    _sheet->addChild(controles);
}

void ControlsState::deleteGUI () {
    _sheet->destroyChild("BackButton");
    _sheet->destroyChild("Controles");
}

bool ControlsState::back (const CEGUI::EventArgs &e) {
    changeState(IntroState::getSingletonPtr());
    return true;
}
