#include "CreditsState.h"
#include "IntroState.h"
#include "Utils.h"

template<> CreditsState* Ogre::Singleton<CreditsState>::msSingleton = 0;

void CreditsState::enter () {
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();

    Utils::getSingletonPtr()->createBackground();
    createGUI();
    _exitGame = false;
}

void CreditsState::exit () {
    deleteGUI();
    Utils::getSingletonPtr()->deleteBackground();
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void CreditsState::pause() {
}

void CreditsState::resume() {
}

bool CreditsState::frameStarted (const Ogre::FrameEvent& evt) {
    return true;
}

bool CreditsState::frameEnded (const Ogre::FrameEvent& evt) {
    if (_exitGame)
        changeState(IntroState::getSingletonPtr());

    return true;
}

void CreditsState::keyPressed (const OIS::KeyEvent &e) {
    /* Tecla C --> IntroState. */
    if (e.key == OIS::KC_C) {
        changeState(IntroState::getSingletonPtr());
    }
}

void CreditsState::keyReleased (const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void CreditsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void CreditsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(Utils::getSingletonPtr()->convertMouseButton(id));
}

void CreditsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(Utils::getSingletonPtr()->convertMouseButton(id));
}

CreditsState* CreditsState::getSingletonPtr () {
    return msSingleton;
}

CreditsState& CreditsState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void CreditsState::createGUI () {
    /* Botón volver menú */
    CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "BackButton");
    backButton->setText("Volver");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.65,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&CreditsState::back, this));
    _sheet->addChild(backButton);

    /* Texto de los créditos */
    CEGUI::Window* credits = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "Credits");
    credits->setText("[font='DejaVuSans-25']Maldonado Burgos, Julia\nMolina Maroto, Antonio");
    credits->setSize(CEGUI::USize(CEGUI::UDim(0.35,0),CEGUI::UDim(0.15,0)));
    credits->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.35/2,0),CEGUI::UDim(0.5-0.15/2,0)));
    _sheet->addChild(credits);
}

void CreditsState::deleteGUI () {
    _sheet->destroyChild("BackButton");
    _sheet->destroyChild("Credits");
}

bool CreditsState::back (const CEGUI::EventArgs &e) {
    changeState(IntroState::getSingletonPtr());
    return true;
}
