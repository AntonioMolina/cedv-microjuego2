#include "RecordsState.h"
#include "IntroState.h"
#include "Utils.h"

using namespace std;

template<> RecordsState* Ogre::Singleton<RecordsState>::msSingleton = 0;

void RecordsState::enter () {
    _root = Ogre::Root::getSingletonPtr();

    /* Se recupera el gestor de escena y la cámara. */
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();

    Utils::getSingletonPtr()->createBackground();
    createGUI();

    _exitGame = false;
}

void RecordsState::exit () {
    deleteGUI();
    Utils::getSingletonPtr()->deleteBackground();
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void RecordsState::pause() {
}

void RecordsState::resume() {
}

bool RecordsState::frameStarted (const Ogre::FrameEvent& evt) {
    return true;
}

bool RecordsState::frameEnded (const Ogre::FrameEvent& evt) {
    if (_exitGame)
        changeState(IntroState::getSingletonPtr());

    return true;
}

void RecordsState::keyPressed (const OIS::KeyEvent &e) {
    /* Tecla R --> IntroState. */
    if (e.key == OIS::KC_R) {
        changeState(IntroState::getSingletonPtr());
    }
}

void RecordsState::keyReleased (const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void RecordsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void RecordsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(Utils::getSingletonPtr()->convertMouseButton(id));
}

void RecordsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(Utils::getSingletonPtr()->convertMouseButton(id));
}

RecordsState* RecordsState::getSingletonPtr () {
    return msSingleton;
}

RecordsState& RecordsState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void RecordsState::createGUI () {
    /* Botón volver menú */
    CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "BackButton");
    backButton->setText("Volver");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.75,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&RecordsState::back, this));
    _sheet->addChild(backButton);

    stringstream rcdsString;
    rcdsString.str("");

    rcdsString << "[colour='FFFFFFFF'][font='DejaVuSans-25']Puesto      Puntos\n";

    string s = createStringRecords();
    vector<string> rcds = split(s, '\n');

    for (uint i = 1; i <= rcds.size(); i++) {
        string aux = rcds[i - 1];
        vector<string> v = split(aux, ' ');
        rcdsString << v[0] << "              " << v[1] << "\n";
    }

    CEGUI::Window* recordsLabel = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "RecordsLabel");
    recordsLabel->setText(rcdsString.str());
    recordsLabel->setSize(CEGUI::USize(CEGUI::UDim(0.45,0),CEGUI::UDim(0.45,0)));
    recordsLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.45/2,0),CEGUI::UDim(0.15,0)));
    _sheet->addChild(recordsLabel);
}

void RecordsState::deleteGUI () {
    _sheet->destroyChild("BackButton");
    _sheet->destroyChild("RecordsLabel");
}

bool RecordsState::back (const CEGUI::EventArgs &e) {
    changeState(IntroState::getSingletonPtr());
    return true;
}

void RecordsState::saveRecords (int puntuation) {
	string line;
    fstream recordsFile;

    vector<int> records;
    records.reserve(5);

    recordsFile.open("records.rcd", ios::in | ios::out | ios::binary);

    if (recordsFile.good()) {
        while (getline(recordsFile, line)) {
            if (line.size() > 0) {
                try {
                    int puntos = atoi(line.c_str());
                    records.push_back(puntos);
                } catch (...) {
                    cout << "No se ha podido leer el dato. Error" << endl;
                }
            }
        }
    }

    recordsFile.close();

    records.push_back(puntuation);

    sort(records.begin(), records.end(), std::greater<int>());

    recordsFile.open("records.rcd", ios::out | ios::trunc | ios::binary);

    for (unsigned int i = 0; i < records.size(); i++) {
        if (i < 5) {
            recordsFile << records.at(i) << endl;
        } else {
            break;
        }
    }
    /*for (vector<int>::iterator it = records.begin(); it != records.end(); ++it) {
        recordsFile << *it << endl;
    }*/

    recordsFile.close();
}

void RecordsState::createRecordsFile () {
    fstream recordsFile;
    recordsFile.open("records.rcd", ios::in | ios::out | ios::binary);
    if (!recordsFile.good()) {
        recordsFile.close();
        recordsFile.open("records.rcd", ios::out | ios::trunc | ios::binary);

        recordsFile.close();
    } else {
        recordsFile.close();
    }
}

string RecordsState::createStringRecords () {
    stringstream res;
    res.str("");
    fstream recordsFile;
    string line;

    vector<int> records;
    records.reserve(5);

    recordsFile.open("records.rcd", ios::in | ios::out | ios::binary);

    int i;
    if (recordsFile.good()) {
        /* Se guardan primero en un vector */
        i = 0;
        while (getline(recordsFile, line)) {
            /* Solo se leen los 5 primeros datos. */
            if (i < 5) {
                if (line.size() > 0) {
                    try {
                        int puntos = atoi(line.c_str());
                        records.push_back(puntos);
                        i++;
                    } catch (...) {
                        cout << "No se ha podido leer el dato. Error" << endl;
                    }
                } else {
                    records.push_back(0);
                    i++;
                }
            }
        }

        while (i < 5) {
            records.push_back(0);
            i++;
        }

        i = 0;
        while (i < 5) {
            res << (i + 1) << " " << records.at(i) << "\n";
            i++;
        }
    }

    recordsFile.close();

    return res.str();
}

/* Código obtenido de la red. */
vector<string> RecordsState::split (string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str);
    string tok;

    while(getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }

    return internal;
}
