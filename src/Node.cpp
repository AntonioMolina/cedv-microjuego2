/* **********************************************************
** Importador NoEscape 1.0
** Curso de Experto en Desarrollo de Videojuegos
** Escuela Superior de Informatica - Univ. Castilla-La Mancha
** Carlos Gonzalez Morcillo - David Vallejo Fernandez
************************************************************/

#include <Node.h>

Node::Node ()
{
    _visitado = false;
    _tipoBola = noBola;
}

Node::Node
(const int& index, const string& type, const Ogre::Vector3& position)
{
  _index = index;
  _type = type;
  _position = position;

  _visitado = false;
  _tipoBola = noBola;
}

Node::Node
(const Node& node)
{
  _index = node.getIndex();
  _type = node.getType();
  _position = node.getPosition();
  _visitado = node.getVisitado();
  _tipoBola = node.getTipoBola();
  _idBola = node.getIdBola();
}

Node::~Node ()
{
}

Node::operator
std::string() const
{
  std::stringstream r;
  r << "[Node: " << _index << " Type: " << _type << " ("
    << _position.x << ", " << _position.y << ", "
    << _position.z << ")]";
  return r.str();
}
